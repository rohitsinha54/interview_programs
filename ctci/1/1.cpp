/* CTCI 1.1) Implement an algorithm to determine if a string has all unique
 characters What if you can not use additional data structures?
 */

#include<bitset>
#include<string>
#include<iostream>

using namespace std;

bool unique_string (string str)
{
    // check string length to be 256 for ascii chars
    if(str.size() > 256)
        return false;
    
    if(str.size() <= 1)
        return false;
    
    bitset<256> bset;
    
    for(int i=0; i<=(str.size()); i++)
    {
        if(bset.test(str[i]))
            return false;
        else
            bset.set(str[i]);
    }
    return true;
}

int main (int argc, char *argv[])
{
    if(argc!=2)
    {
        cout << "Usage: Please pass the string to be checked." << endl;
    }
    else
    {
        bool ret = unique_string(argv[1]);
        
        if(ret)
        {
            cout << "Checker: All chars are unique" << endl;
        }
        else
        {
            cout << "Checker: Chars are not unique" << endl;
            
        }
        
    }
    
}

