/*
 CTCI 1.6) Given an image represented by an NxN matrix, where each pixel in the
 image is 4 bytes, write a method to rotate the image by 90 degrees Can you do this 
 in place?
 */

#include <iostream>
#include <math.h>

#define SIZE 5

using namespace std;

void turn90 (int p[SIZE][SIZE])
{
    int temp;
    for (int i=0; i<SIZE/2;i++)
    {
        for (int j=i; j<SIZE-1-i; j++)
        {
            temp=p[i][j];
            p[i][j]= p[SIZE-1-j][i];
            p[SIZE-1-j][i] = p[SIZE-1-i][SIZE-1-j];
            p[SIZE-1-i][SIZE-1-j]=p[j][SIZE-1-i];
            p[j][SIZE-1-i]=temp;
        }
    }
}

int main()
{
    int mat[SIZE][SIZE]={{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5}};
    
    turn90(mat);
    
    for(int i=0; i<SIZE; i++)
    {
        for(int j=0; j<SIZE; j++)
        {
            cout << mat[i][j];
            if(j==SIZE-1)
                cout << endl;
        }
    }
    return 0;
}
