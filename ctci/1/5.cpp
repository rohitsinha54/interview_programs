/*
 * From Cracking the Coding Interview, 5th edition
 * Problem 1.4
 *
 * Write a method to replace all spaces in a string with "%20". You may
 * assume that the string has sufficient space at the end of the string
 * to hold the additional chararcters, and that you are given the "true"
 * length of the string.
 */

#include <iostream>
#include <cstring>
#include <stdexcept>

using namespace std;

void sp_replace (char* str, int l)
{
    if (str == NULL)
        throw invalid_argument ("NULL String");
    if (l == 0) return;
    
    // total number of spaces
    int sp_count = 0;
    for(int i=0; i<l; i++)
        if(str[i] == ' ')
            sp_count++;
    
    int l_pos = (l+2*sp_count);
    str[l_pos--]='\0';
    while(sp_count > 0)
    {
        if(str[l_pos-2*sp_count] != ' ')
        {
            str[l_pos] = str[l_pos-2*sp_count];
            l_pos--;
        }
        else
        {
            str[l_pos--] = '0';
            str[l_pos--] = '2';
            str[l_pos--] = '%';
            
            sp_count --;
        }
    }
}

int main (int argc, char *argv[])
{
    char buf[1024];
    
    if(argc!=2)
        cout << "Usage: Please pass the string" << endl;
    
    
    else
    {
        strcpy(buf, argv[1]);
        try
        {
            sp_replace(buf,strlen(buf));
        }
        catch (invalid_argument e)
        {
            cout << "Error: " << e.what() << endl;
        }
        
        cout << "Replacer: The new string is:" << buf << endl;
    }
    
}

