/*
 * From Cracking the Coding Interview, 5th edition
 * Problem 1.2
 *
 * Implement a function void reverse(char* str) in C or C++ which reverses
 * a null-terminated string.
 */


#include <cstring>
#include <iostream>

using namespace std;

void reverser (char* str)
{
    if(*str)
    {
        int l = strlen(str);
        
        for (int i=0; i<l/2; i++)
        {
            char temp = str [i];
            str[i] = str[l-i-1];
            str[l-i-1] = temp;
        }
    }
}

int main (int argc, char *argv[])
{
    char buf[1024];
    
    if(argc!=2)
    {
        cout << "Usage: Please pass the string to be checked." << endl;
    }
    else
    {
        strcpy(buf,argv[1]);
        reverser(buf);
        
        cout << "Reverser: The reversed string is: " << buf << endl;
        
    }
}
