/*
 CTCI 1.7) Write an algorithm such that if an element in an MxN matrix is 0, its
 entire row and column is set to 0
 */

#include <iostream>
#include <unordered_set>
#include <stdexcept>

using namespace std;

void set_zero(int* mat, int M, int N)
{
    if(mat==NULL) throw invalid_argument ("Matrix is empty");
    if(M<=0 || N<=0) throw invalid_argument ("Invalid Matrix");
    
    unordered_set<int> c, r;
    
    for(int i=0; i<M; i++)
    {
        for(int j=0; j<N; j++)
        {
            if(mat[i*N+j] == 0)
            {
                r.insert(i);
                c.insert(j);
            }
        }
    }
    
    for(unordered_set<int>::const_iterator SI = r.begin(); SI!=r.end(); ++SI)
        for(int i=0; i<N; i++)
            mat[*SI*N+i]=0;
    for(unordered_set<int>::const_iterator SI = c.begin(); SI!=c.end(); ++SI)
        for(int i=0; i<M; i++)
            mat[i*N+*SI]=0;
}

void print_matrix(int* s, int M, int N) {
	if (s == NULL) throw invalid_argument("NULL pointer");
	if (M <= 0 || N <= 0) throw invalid_argument("Invalid matrix");
    
	for (int i=0; i<M; i++) {
		for (int j=0; j<N; j++) {
			cout.width(3);
			cout << s[i*N+j];
		}
		cout << endl;
	}
	cout << endl;
}

int main()
{
    int M = 4, N = 3;
    
    int* matx = new int[M*N];
    
    for (int i=0; i<M*N; i++) matx[i] = rand()%5;
    
    print_matrix(matx, M, N);
	set_zero(matx, M, N);
	print_matrix(matx, M, N);
    
    delete[] matx;
    matx = NULL;
    
}
