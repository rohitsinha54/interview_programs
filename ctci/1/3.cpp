/*
 * From Cracking the Coding Interview, 5th edition 
 * Problem 1.3
 *
 * Given two strings, write a method to decide if one is a permutation
 * of the other.
 */
 
#include <cstring>
#include <unordered_map>
#include <stdexcept>
#include <iostream>

typedef wchar_t CHAR;

using namespace std;

bool str_perm (char* str1, char* str2)
{
    if(str1 == NULL ||str2 == NULL)
    {
        throw invalid_argument ("One string is null");
    }
    
    int l = strlen(str1);
    if (l != strlen(str2)) 
        return false;
    if (l == 0)
        return true;
        
    unordered_map<CHAR, int> count1, count2;
    for(int i=0; i<l; i++)
    {
        if(count1.find(str1[i]) == count1.end())
            count1[str1[i]]=1;
        else
            count1[str1[i]]++;
        if(count2.find(str2[i]) == count2.end())
            count2[str2[i]]=1;
        else
            count2[str2[i]]++;
    }
    
    if(count1.size() != count2.size())
        return false;
    unordered_map<CHAR, int>::const_iterator MI;
    for(MI=count1.begin(); MI!=count1.end(); MI++)
    {
        if(MI->second != count2[MI->first])
            return false;
    }
    return true;
}

int main (int argc, char *argv[])
{
    if(argc != 3)
    {
        cout << "Usage: Please pass the two string" << endl;
    }
    else
    {
        bool ret = str_perm(argv[1], argv[2]);
        
        if(ret)
        {
            cout << "String 2 is permutation of String 1" << endl;
        }
        else
        {
            cout << "String 2 is not a permutation of String 1" << endl;
        }
    }
}
