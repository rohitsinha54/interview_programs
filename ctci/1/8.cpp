/*
 Assume you have a method isSubstring which checks if one word is a substring of
 another Given two strings, s1 and s2, write code to check if s2 is a rotation of
 s1 using only one call to isSubstring (i e , “waterbottle” is a rotation of
 “erbottlewat”)
 */

#include <iostream>
#include <cstring>
using namespace std;
bool isRotation (char* string1, char* string2)
{
    int l = strlen(string1);
    if(l!=strlen(string2))
        return false;
    if (l == 0)
        return true;
    
    char str[l*2+1];
    
    strcpy(str,string2);
    strcat(str,string2);
    if(strstr(str, string1))
        return true;
    else
        return false;
    
    
}

int main(int argc, char *argv[])
{
    
    if(argc != 3)
    {
        cout << "Usage: Please pass the two string to be checked" << endl;
    }
    else
    {
        bool ret_value = isRotation(argv[1],argv[2]);
        
        if(ret_value)
            cout << "Is Rotation" << endl;
        else
            cout << "Is not Rotation" << endl;
    }
    return 0;
}
