#include <iostream>
#include <deque>
#include <climits>

using namespace std;

struct Tree
{
	char data;
	Tree *left;
	Tree *right;
	Tree *parent;
};

struct Tree* newTreeNode (int data)
{
	struct Tree *node = new Tree;
	node->data = data;
	node->left = NULL;
	node->right = NULL;
	node->parent = NULL;
    
	return node;
}

struct Tree* insertTreeNode (struct Tree *node, char data)
{
    struct Tree* new_node;
    if(node == NULL)
    {
        new_node = newTreeNode(data);
        return new_node;
    }
    else
    {
        if(data <= node->data)
        {
            node->left = insertTreeNode(node->left,data);
        }
        else
        {
            node->right = insertTreeNode(node->right,data);
        }
        
    }
    return node;
}

void printTreeInorder(struct Tree* node)
{
    if(node == NULL)
        return;
    
    printTreeInorder(node->left);
    cout << node->data << " ";
    printTreeInorder(node->right);
}

void printTreePreorder (struct Tree* node)
{
    
    if(node == NULL)
        return;
    
    cout << node->data << " ";
    printTreePreorder(node->left);
    printTreePreorder(node->right);
}

void printTreePostorder (struct Tree* node)
{
    if (node == NULL)
        return;
    
    printTreePostorder(node->left);
    printTreePostorder(node->right);
    cout << node->data << " ";
}

bool isBST (struct Tree* node)
{
    static struct Tree* prev_node = NULL;
    
    if(node == NULL)
    {
        return true;
    }
    
    isBST(node->left);
    
    if(prev_node != NULL && node->data <= prev_node->data)
        return false;
    
    prev_node = node;
    
    isBST(node->right);
    return true;
}

struct Tree* lookUp (struct Tree* node, char key)
{
    if(node == NULL)
        return NULL;
    
    if(node->data == key)
        return node;
    else if(key < node->data)
        return lookUp(node->left, key);
    else
        return lookUp(node->right, key);
}

struct Tree* leftMost (struct Tree* node)
{
    if (node == NULL)
        return NULL;
    while(node->left != NULL)
        node = node->left;
    return node;
}



struct Tree* rightMost (struct Tree* node)
{
    if (node == NULL)
        return NULL;
    while(node->right != NULL)
        node = node->right;
    return node;
}

int treeSize (struct Tree* node)
{
    if(node == NULL)
        return 0;
    else
        return treeSize(node->left) + 1 + treeSize(node->right);
}

int maxDepth (struct Tree* node)
{
    if(node == NULL || (node->left == NULL && node->right == NULL))
        return 0;
    
    int max_left_depth = maxDepth(node->left);
    int max_right_depth = maxDepth(node->right);
    
    return max_left_depth > max_right_depth?max_left_depth+1:max_right_depth+1;
}

int minDepth (struct Tree* node)
{
    if(node == NULL || (node->left == NULL && node->right == NULL))
        return 0;
    
    int min_left_depth = minDepth(node->left);
    int min_right_depth = minDepth(node->right);
    
    return min_left_depth < min_right_depth?min_left_depth+1:min_right_depth+1;
}

bool isBalanced (struct Tree* node)
{
    if(maxDepth(node) - minDepth(node) <= 1)
        return true;
    else
        return false;
}

struct Tree* minTree (struct Tree* node)
{
    if(node == NULL)
        return NULL;
    
    while(node->left)
        node = node->left;
    
    return node;
}

struct Tree* maxTree (struct Tree* node)
{
    if(node == NULL)
        return NULL;
    
    while(node->right)
        node = node->right;
    
    return node;
}




int main (int argc, char *argv[])
{
    int choice;
    char data, key;
    
    struct Tree *root=NULL, *temp;
    
    do
    {
        
        cout << "== Trees in C++ ==" << endl;
        cout << "1. Insert a node in tree" << endl;
        cout << "2. Inorder Traversal" << endl;
        cout << "3. Preorder Traversal" << endl;
        cout << "4. Postorder Traversal" << endl;
        cout << "5. Check for Binary Search Tree" << endl;
        cout << "6. Look up in tree" << endl;
        cout << "7. Left most node in Tree" << endl;
        cout << "8. Right most node in Tree" << endl;
        cout << "9. Size of the tree" << endl;
        cout << "10. Max depth of tree" << endl;
        cout << "11. Min depth of tree" << endl;
        cout << "12. Check if tree is balanced" << endll
        cout << "13. Minimum node in tree" << endl;
        cout << "14. Maximum node in tree" << endl;
        cout << "0: Exit the program" << endl;
        
        cin >> choice;
        
        switch(choice)
        {
            case 0:
                break;
            case 1:
                cout << "Enter the character to be inserted" << endl;
                cin >> data;
                
                root = insertTreeNode(root, data);
                
                break;
            case 2:
                cout << "Inorder:";
                
                printTreeInorder(root);
                cout << endl;
                
                break;
            case 3:
                cout << "Preorder:";
                
                printTreePreorder(root);
                cout << endl;
                
                break;
            case 4:
                cout << "PostOrder:";
                
                printTreePostorder(root);
                cout << endl;
                
                break;
            case 5:
                if(isBST(root))
                {
                    cout << "The tree is BST" << endl;
                }
                else
                {
                    cout << "The tree is not BST" << endl;
                }
                break;
            case 6:
                cout << "Enter the character to be searched" << endl;
                cin >> key;
                
                temp = lookUp(root, key);
                
                if(temp)
                    cout << "Key found in binary tree" << endl;
                else
                    cout << "Key not found in binary tree" << endl;
                break;
            case 7:
                temp = leftMost(root);
                
                if(temp)
                    cout << "Left most:" << temp->data << endl;
                else
                    cout << "Tree is empty";
                break;
            case 8:
                temp = rightMost(root);
                
                if(temp)
                    cout << "Right most:" << temp->data << endl;
                else
                    cout << "Tree is empty";
                break;
            case 9:
                cout << "Size of tree: " << treeSize(root) << endl;
                break;
            case 10:
                cout << "Max depth: " << maxDepth(root) << endl;
                break;
            case 11:
                cout << "Min depth: " << minDepth(root) << endl;
                break;
            case 12:
                if(isBalanced(root))
                    cout << "The tree is balanced" << endl;
                else
                    cout << "The tree is not balanced" << endl;
                break;
            case 13:
                if(temp = minTree(root))
                    cout << "Minimum in tree: " << temp->data << endl;
                else
                    cout << "Tree is empty" << endl;
                break;
            case 13:
                if(temp = maxTree(root))
                    cout << "Maximum in tree: " << temp->data << endl;
                else
                    cout << "Tree is empty" << endl;
                break;              
            default:
                cout << "Invalid choice" << endl;
                
        }
        
    }while(choice!=0);
    
    
}

