/*
 CTCI 4.5) Write an algorithm to find the "next" node (i.e. in-order sucessor) of
 a given node in a binary search tree. You may assume each node has link to its
 parent
 */

#include <iostream>

using namespace std;

struct Node
{
    int value;
    Node *left;
    Node *right;
    Node *parent;
    Node(int v=0, Node *l=NULL, Node *r=NULL, Node *p=NULL)
    {
        value=v; l=left; r=right, p=NULL;
    }
};

struct Node* minTree (struct Node* node)
{
    if(node==NULL)
        return NULL;
    
    while(node->left != NULL)
        node = node->left;
    
    return node;
}

struct Node* inOrderSuccessor (struct Node *node)
{
    if(node == NULL)
        return NULL;
    
    if(node->right != NULL)
        return(minTree(node->right));
    
    Node *temp = node->parent;
    
    while(temp!=NULL && node==temp->right)
    {
        node = temp;
        temp = temp->parent;
    }
    return temp;
}

int main()
{
    Node *root = new Node(4);
    Node *l1 = new Node(1);
    Node *r1 = new Node(6);
    Node *l2 = new Node(0);
    Node *r2 = new Node(3);
    Node *l3 = new Node(5);
    Node *r3 = new Node(7);
    Node *l4 = new Node(2);
    root -> left = l1;
    root -> right = r1;
    l1 -> parent = root;
    r1 -> parent = root;
    l1 -> left = l2;
    l1 -> right = r2;
    l2 -> parent = l1;
    r2 -> parent = l1;
    r1 -> left = l3;
    r1 -> right = r3;
    l3 -> parent = r1;
    r3 -> parent = r1;
    // l2 -> right = r2;
    r2 -> left = l4;
    l4 -> parent = r2;
    Node *result = inOrderSuccessor(r2);
    if (result != NULL)
        cout << result -> value << endl;
    return 0;
}


