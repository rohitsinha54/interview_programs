/*
 CTCI 4.6) Design an algorithm and write code to find the first common ancestor of
 two nodes in a binary tree Avoid storing additional nodes in a data structure
 NOTE: This is not necessarily a binary search tree
 */

#include <iostream>

using namespace std;

struct Node
{
    int value;
    Node *left;
    Node *right;
    Node(int v=0, Node *l=NULL, Node *r=NULL)
    {
        value=v; left=l;right=r;
    }
};

struct Node* commonAncestor (struct Node *root, struct Node *p, struct Node *q)
{
    if (root == NULL)
        return NULL;
    struct Node *left, *right;
    
    if(root->left == p || root->right == p || root->left == q ||
       root->right == q)
        return root;
    else
    {
        left = commonAncestor(root->left, p, q);
        right = commonAncestor(root->right, p, q);
        
        if(left && right)
            return root;
        else
            return (left)?left:right;
    }
}

int main()
{
    Node *root = new Node(4);
    Node *l1 = new Node(1);
    Node *r1 = new Node(6);
    Node *l2 = new Node(0);
    Node *r2 = new Node(3);
    Node *l3 = new Node(5);
    Node *r3 = new Node(7);
    Node *l4 = new Node(2);
    root -> left = l1;
    root -> right = r1;
    l1 -> left = l2;
    l1 -> right = r2;
    r1 -> left = l3;
    r1 -> right = r3;
    // l2 -> right = r2;
    r2 -> left = l4;
    Node *result = commonAncestor(root, l4, l3);
    if (result != NULL)
        cout << result -> value << endl;
    return 0;
}
