/*******************************
You are given a binary tree in which each node contains a value. Design an 
algorithm to print all paths which sum to a given value. Note that a path can 
start or end anywhere in the tree.
*******************************/

#include <iostream>
#include <vector>

using namespace std;

struct Node
{
    int value;
    Node* left;
    Node *right;
    Node(int v=0, Node *l=NULL, Node *r=NULL)
    {
        value=v; left=l; right=r;
    }
};

void sumPath (Node *root, int sum, vector<int> v)
{
    if(root == NULL)
        return;

    v.push_back(root->value);
    int cur_sum=0;

    for(int i=v.size()-1; i>=0; i--)
    {
        cur_sum += v[i];
        
        if(cur_sum == sum)
        {
            for(int j=i; j<v.size(); j++)
                cout << v[j] << "->";
            cout << endl;
        }
    }
    sumPath(root->left,sum,v);
    sumPath(root->right,sum,v);
}

int main()
{
    vector<int> v;
      Node *root = new Node(4);
        Node *l1 = new Node(1);
          Node *r1 = new Node(6);
            Node *l2 = new Node(0);
              Node *r2 = new Node(3);
                Node *l3 = new Node(5);
                  Node *r3 = new Node(7);
                    Node *l4 = new Node(2);
                      root -> left = l1;
                        root -> right = r1;
                          l1 -> left = l2;
                            l1 -> right = r2;
                              r1 -> left = l3;
                                r1 -> right = r3;
                                  // l2 -> right = r2;
                                    r2 -> left = l4;
                                      sumPath(root, 5, v);
                                        return 0;
}
