/*
 CTC 4.3) Given a sorted (increasing order) array, write an algorithm to create a binary tree with minimal height
 */
#include <iostream>

using namespace std;

struct Node
{
    int value;
    Node* left;
    Node* right;
    
    Node(int v=0, Node *l=NULL, Node *r=NULL)
    {
        value=v; left=l; right=r;
    }
};

Node* createBST (int *arr, int start, int end)
{
    if(start>end)
        return NULL;
    
    int mid = (end + start)/2;
    
    Node *root = new Node(arr[mid]);
    
    root->left = createBST(arr, start, mid-1);
    root->right = createBST(arr, mid+1, end);
    
    return root;
}
void printTreeInorder(struct Node* node)
{
    if(node == NULL)
        return;
    
    printTreeInorder(node->left);
    cout << node->value << " ";
    printTreeInorder(node->right);
}

int main ()
{
    int arr[10];
    
    for(int i=0; i<10; i++)
        arr[i]=i;
    
    struct Node *root = createBST(arr, 0, (sizeof(arr)/sizeof(int)-1));
    
    printTreeInorder(root);
}


