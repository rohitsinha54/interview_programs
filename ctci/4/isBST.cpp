/*
Implement a function to check if the given tree is a binary search tree
*/

#include <iostream>

using namespace std;

struct Node
{
    int value;
    Node *left;
    Node *right;
    Node (int v=0, Node *l=NULL, Node *r=NULL)
    {
        value=v; left=l; right=r;
    }
};

bool isBST (struct Node *node)
{
    static struct Node *prev = NULL;
    if(node)
    {
            
        if(!isBST(node->left))
            return false;

        if(prev != NULL && prev->value >= node->value)
            return false;
    
        prev = node;

        return (isBST(node->right));
    }
    return true;
}

int main()
{
      Node *root = new Node(4);
        Node *l1 = new Node(1);
          Node *r1 = new Node(6);
            Node *l2 = new Node(0);
              Node *r2 = new Node(3);
                Node *l3 = new Node(5);
                  Node *r3 = new Node(7);
                    Node *l4 = new Node(2);
                      root -> left = l1;
                        root -> right = r1;
                          l1 -> left = l2;
                            l1 -> right = r2;
                              r1 -> left = l3;
                                r1 -> right = r3;
                                  //l2 -> right = r2;
                                    r2 -> left = l4;
                                      cout << isBST(root) << endl;
                                        return 0;
}
