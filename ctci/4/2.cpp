/*
 CTCI 4.2 - Given a directed graph, design an algorithm to find out whether
 there is a route between two nodes.
 */

#include <iostream>
#include <vector>
#include <map>
#include <queue>

using namespace std;

struct Node
{
    int value;
    vector<Node *> nghbr;
    Node (int v = 0) {value =v;}
};

bool DFS (Node *src, Node *dest, map<Node *, int> &m)
{
    if(src=dest)
        return true;
    
    for(size_t i=0; i<src->nghbr.size();++i)
    {
        map<Node *, int>::iterator it = m.find(src->nghbr[i]);
        if(it != m.end())
            continue;
        else
        {
            m.insert(pair<Node *, int> (src->nghbr[i],0));
            if(DFS(src->nghbr[i],dest,m))
                return true;
        }
    }
    return false;
}

bool has_route (Node* src, Node* des)
{
    if(src == NULL || des == NULL)
        return false;
    map<Node *,int> m;
    return (DFS(src,des,m));
}

int main()
{
    Node *a = new Node(1);
    Node *b = new Node(2);
    Node *c = new Node(3);
    Node *d = new Node(4);
    Node *e = new Node(5);
    
    a -> nghbr.push_back(b);
    a -> nghbr.push_back(c);
    a -> nghbr.push_back(d);
    b -> nghbr.push_back(a);
    b -> nghbr.push_back(c);
    c -> nghbr.push_back(a);
    c -> nghbr.push_back(b);
    c -> nghbr.push_back(e);
    d -> nghbr.push_back(a);
    e -> nghbr.push_back(c);
    cout <<  has_route(d, e) << endl;
    return 0;
}

