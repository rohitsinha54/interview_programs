/*
 Implement a function to check if a tree is balanced. For the purposes of
 this question, a balanced tree is defined to be a tree such that no two leaf
 nodes differ in distance from the root by more than one.
 */

struct Tree
{
    int data;
    Tree* left;
    Tree* right;
};


int maxDepth (struct Tree* node)
{
    if(node == NULL || (node->left==NULL && node->right == NULL))
        return 0;
    
    int left_depth = maxDepth(node->left)
    int right_depth = maxDepth(node->right);
    
    return left_depth > right_depth?left->depth+1:right->depth+1;
}

int minDepth (struct Tree* node)
{
    if (node == NULL || (node->left == NULL && node->right == NULL))
        return 0;
    
    int left_depth = maxDepth(node->left)
    int right_depth = maxDepth(node->right);
    
    return left_depth < right_depth?left_depth+1:right_depth+1;
}

bool isBalanced (struct Tree* node)
{
    if((maxDepth(node) - minDepth(node)) <= 1 )
        return true;
    else
        return false;
}
