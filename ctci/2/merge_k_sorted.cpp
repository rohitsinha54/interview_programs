#include <iostream>
#include <queue>

using namespace std;

struct ListLinkedNode {
    int val;
    ListLinkedNode *next;
    ListLinkedNode(int x) : val(x), next(NULL) {}
    };
    
class comp{
public:
    bool operator() (const ListLinkedNode* a, const ListLinkedNode* b)    { 
        return (a->val > b->val);
    }
};

class Solution {
public:
    ListLinkedNode *mergeKLists(vector<ListLinkedNode*> &lists) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        int k = lists.size();
        if(k==0) return NULL;
        bool maxheap = false;
        priority_queue<ListLinkedNode*, vector<ListLinkedNode*>,comp> myheap;
        ListLinkedNode * output,*cur;
        for(int i=0;i<k;i++){
            if(lists[i]!=NULL) myheap.push(lists[i]);
        }
        if(myheap.empty()) return NULL;
        output = myheap.top();
        myheap.pop();
        cur = output;
        if(cur->next != NULL) myheap.push(cur->next);
        while(!myheap.empty()){
            cur->next = myheap.top();
            myheap.pop();
            cur = cur->next;
            if(cur->next != NULL) myheap.push(cur->next);
        }
        return output;
    }
};

int main()
{
    vector<ListLinkedNode*> mylist;
    Solution sol;
    
  ListLinkedNode *a = new ListLinkedNode(1);
  ListLinkedNode *b = new ListLinkedNode(2);
  ListLinkedNode *c = new ListLinkedNode(3);
  ListLinkedNode *d = new ListLinkedNode(4);
  ListLinkedNode *e = new ListLinkedNode(5);
  a -> next=b;
  b -> next=c;
  c -> next=d;
  d -> next=e;
  
  ListLinkedNode *f = new ListLinkedNode(6);
  ListLinkedNode *g = new ListLinkedNode(7);
  ListLinkedNode *h = new ListLinkedNode(8);
  ListLinkedNode *i = new ListLinkedNode(9);
  ListLinkedNode *j = new ListLinkedNode(10);
  f -> next=g;
  g -> next=h;
  h -> next=i;
  i -> next=j;
  
  ListLinkedNode *k = new ListLinkedNode(20);
  ListLinkedNode *l = new ListLinkedNode(21);
  ListLinkedNode *m = new ListLinkedNode(22);
  ListLinkedNode *n = new ListLinkedNode(23);
  ListLinkedNode *o = new ListLinkedNode(25);
  k -> next=l;
  l -> next=m;
  m -> next=n;
  n -> next=o;
  
  mylist.push_back(a);
  mylist.push_back(f);
  mylist.push_back(k);
  
  ListLinkedNode *output = sol.mergeKLists(mylist);
  
  while(output!=NULL)
  {
    cout << output->val <<" ";
    output= output->next;
  }
  
  
}
