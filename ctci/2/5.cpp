#include <iostream>
#include <stack>
#include <cstdlib>
#include <ctime>
#include <stdexcept>

using namespace std;

struct node {
    int num;
    node* next;
    node(int _num):num(_num), next(NULL) {}
};

void insert_list(node** head, int n) {
    node* temp = new node(n);
    temp->next = *head;
    *head = temp;
}

void print_list(node* head) {
    while (head != NULL) {
        cout << head->num;
        head = head->next;
    }
    cout << endl;
}

void delete_list(node* head) {
    while (head != NULL) {
        node* temp = head;
        head = head->next;
        delete temp;
    }
}

node* reverse_add(node* h1, node* h2) {
    if (h1 == NULL || h2 == NULL) throw invalid_argument("NULL pointer");

    node* h = new node(0);
    node* hp = h;
    int carry = 0;
    int n1, n2;
    while (h1 != NULL || h2 != NULL) {
        if (h1 != NULL) {
            n1 = h1->num;
            h1 = h1->next;
        } else n1 = 0;
        if (h2 != NULL) {
            n2 = h2->num;
            h2 = h2->next;
        } else n2 = 0;
        if (n1 < 0 || n1 > 9 || n2 < 0 || n2 > 9) 
            throw invalid_argument("Invalid digits");

        int n = n1+n2+carry;
        hp->num = n%10;
        carry = n/10;
        if (!(h1 == NULL && h2 == NULL && carry == 0)) {
            hp->next = new node(0);
            hp = hp->next;
        }
    }
    if (carry == 1) hp->num = 1;
    return h;
}

node* forward_add(node* h1, node* h2) {
    if (h1 == NULL || h2 == NULL) throw invalid_argument("NULL pointer");

    stack<node*> s1;
    stack<node*> s2;
    while (h1 != NULL) {
        if (h1->num < 0 || h1->num > 9) 
            throw invalid_argument("Invalid digits");

        s1.push(h1);
        h1 = h1->next;
    }
    while (h2 != NULL) {
        if (h2->num < 0 || h2->num > 9)
            throw invalid_argument("Invalid digits");

        s2.push(h2);
        h2 = h2->next;
    }
    node* head = NULL;
    int carry = 0;
    while (!s1.empty() || !s2.empty()) {
        int n1, n2, n;
        if (!s1.empty()) {
            n1 = s1.top()->num;
            s1.pop();
        } else n1 = 0;
        if (!s2.empty()) {
            n2 = s2.top()->num;
            s2.pop();
        } else n2 = 0;
        n = n1+n2+carry;
        node* temp = new node(n%10);
        temp->next = head;
        head = temp;
        carry = n/10;
    }
    if (carry == 1) {
        node* temp = new node(1);
        temp->next = head;
        head = temp;
    }
    return head;
}


int main() {
    srand(time(0));
    node* num1 = NULL;
    node* num2 = NULL;
    for (int test=0; test<10; test++) {
        int digit1 = rand()%10;
        int digit2 = rand()%10;
        for (int i=0; i<digit1; i++) insert_list(&num1, rand()%10);
        for (int i=0; i<digit2; i++) insert_list(&num2, rand()%10);
        node* num3 = NULL;
        node* num4 = NULL;
        try {
            num3 = reverse_add(num1, num2);
            num4 = forward_add(num1, num2);
            cout << "Number 1    = "; print_list(num1);
            cout << "Number 2    = "; print_list(num2);
            cout << "Reverse sum = "; print_list(num3);
            cout << "Forward sum = "; print_list(num4);
        }
        catch (invalid_argument e) {
            cout << "Error: " << e.what() << endl;
        }
        cout << endl;
        delete_list(num1);
        delete_list(num2);
        delete_list(num3);
        delete_list(num4);
        
        num1 = NULL;
        num2 = NULL;
        num3 = NULL;
        num4 = NULL;
    }
}

