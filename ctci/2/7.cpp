/*
 * From Cracking the Coding Interview, 5th edition
 * Problem 2.7
 *
 * Implement a function to check if a linked list is a palindrome.
 */

#include <iostream>
#include <stack>
#include <cstdlib>
#include <stdexcept>

using namespace std;

struct node {
    int num;
    node* next;
    node(int _num):num(_num), next(NULL) {}
};

void insert_list(node** head, int n) {
    node* temp = new node(n);
    temp->next = *head;
    *head = temp;
}

void print_list(node* head) {
    while (head != NULL) {
        cout << head->num;
        head = head->next;
    }
    cout << endl;
}

void delete_list(node* head) {
    while (head != NULL) {
        node* temp = head;
        head = head->next;
        delete temp;
    }
}

bool if_palindrome(node* head) {
    if (head == NULL) throw invalid_argument("NULL pointer");

    if (head->next == NULL) return true;
    stack<node*> s;
    node* h = head;
    while (h != NULL) {
        s.push(h);
        h = h->next;
    }
    h = head;
    do {
        if (s.top()->num != h->num) return false;
        //if (s.top() == h->next || s.top() == h->next->next) return true;
        s.pop();
        h = h->next;
    } while (!(s.empty()) && h==NULL);
    if(s.empty() && h==NULL)
        return true;
    else
        return false;
}

int main() {
    srand(time(0));
    node* head = NULL;
    for (int test=0; test<50; test++) {
        int length = rand()%50+2;

// Make the test case: For odd tests, let the values of the linked list be
// random,
// so the linked list will be very unlikely to be palindrome.
// For even tests, we manually set the linked list be palindrome.
        for (int i=0; i<length; i++) {
            if ((test+1)%2 == 0) insert_list(&head, i<length/2?i:length-1-i);
            else insert_list(&head, rand());
        }
        cout << "Test ";
        cout.width(3); cout << test+1 << boolalpha;
        cout << ": " << if_palindrome(head) << endl;
        delete_list(head);
        head = NULL;
    }
}

/*
Test   1: false
Test   2: true
Test   3: false
Test   4: true
Test   5: false
Test   6: true
Test   7: false
Test   8: true
Test   9: false
Test  10: true
Test  11: false
Test  12: true
Test  13: false
Test  14: true
Test  15: false
Test  16: true
Test  17: false
Test  18: true
Test  19: false
Test  20: true
Test  21: false
Test  22: true
Test  23: false
Test  24: true
Test  25: false
Test  26: true
Test  27: false
Test  28: true
Test  29: false
Test  30: true
Test  31: false
Test  32: true
Test  33: false
Test  34: true
Test  35: false
Test  36: true
Test  37: false
Test  38: true
Test  39: false
Test  40: true
Test  41: false
Test  42: true
Test  43: false
Test  44: true
Test  45: false
Test  46: true
Test  47: false
Test  48: true
Test  49: false
Test  50: true
*/
