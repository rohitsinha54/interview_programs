#include <iostream>
#include <stack>
#include <cstdlib>
#include <stdexcept>
using namespace std;

list<int> * reverse_ll(list<int> head)
{
      struct node * temp = NULL;

      if ( head == NULL)
          return NULL;
      if ( head->next == NULL )
         return head;

      temp = reverse_ll ( head->next );
      head->next -> next = head;
      head->next = NULL;
      return temp;
}

int main() {
    srand(time(0));
    list<int> l;
    
    l.push_back(0);
    l.push_back(1);
    l.push_back(2);
    l.push_back(3);
    
    list<int>::iterator i;
    
    for(i=l.begin(); i!=l.end(); ++i) cout << *i << " ";
    cout << endl;
    
    l=reverse_ll(l);
    
    for(i=l.begin(); i!=l.end(); ++i) cout << *i << " ";
    cout << endl;
    
    
}

        

