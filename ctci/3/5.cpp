/*
 CTCI 3.5) Implement MyQueue class which implements queue using two stacks.
 */

#include <iostream>
#include <stack>


using namespace std;

template <typename T> class MyQueue
{
private:
    stack<T> main_stack;
    stack<T> aux_stack;
    
public:
    void push(T ele)
    {
        main_stack.push(ele);
    }
    T pop()
    {
        T ele;
        
        if(main_stack.empty() && aux_stack.empty())
        {
            cout << "The Queue is empty" << endl;
            
        }
        if(aux_stack.empty())
        {
            while(!main_stack.empty())
            {
                aux_stack.push(main_stack.top());
                main_stack.pop();
            }
        }
        
        ele = aux_stack.top();
        aux_stack.pop();
        return ele;
    }
};


int main()
{
    MyQueue<int> q;
    
    q.push(10);
    q.push(20);
    q.push(30);
    q.push(40);
    
    cout << q.pop() << endl;
    cout << q.pop() << endl;
    cout << q.pop() << endl;
    cout << q.pop() << endl;
    
    return 0;
}


