/*
How would you design a stack which, in addition to push and pop,
also has a function min which returns the minimum element?
Push, pop and min should all operate in O(1)time
*/

#include <iostream>
#include <stack>

#define MAX_SIZE 1024

using namespace std;

// Simple stack class
class Stack{
    private:
    stack<int> main_stack;
    stack<int> aux_stack;

    public:
    bool empty();
    bool full();
    int pop();
    int getMin();
    void push(int x);
};

// Function to check if stack is empty
bool Stack::empty()
{
    if(main_stack.empty())
        return true;
    return false;
}

// Function to check is stack is full
bool Stack::full()
{
    if(main_stack.size()==MAX_SIZE-1)
        return true;
    return false;
}

void Stack::push(int ele)
{
    if(main_stack.empty())
    {
        main_stack.push(ele);
        aux_stack.push(ele);
    }
    else
    {
        main_stack.push(ele);
        int temp = aux_stack.top();
        

        // push to min stack only when incoming element is <=
        if(ele <= temp)
            aux_stack.push(ele);
    }
}

int Stack::getMin()
{
    int min_stack = aux_stack.top();
    return min_stack;
}

int Stack::pop()
{
    int top_stack = main_stack.top();
    int top_min = aux_stack.top();
    main_stack.pop();
    aux_stack.pop();
    
    // Push the element only when equal
    if(top_stack!=top_min)
        aux_stack.push(top_min);
    return(top_stack);
    
}


int main()
{
        Stack s;
            s.push(10);
                s.push(20);
                    s.push(30);
                        cout<<s.getMin()<<endl;
                            s.push(5);
                                cout<<s.getMin();
                                    return 0;
}
