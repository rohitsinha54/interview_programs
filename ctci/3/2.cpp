/*
 How would you design a stack which, in addition to push and pop,
 also has a function min which returns the minimum element?
 Push, pop and min should all operate in O(1)time
 */

#include <iostream>

#define MAX_SIZE 1024

using namespace std;

// Simple stack class
class Stack{
private:
    int arr[MAX_SIZE];
    int top;
    
public:
    Stack() { top=-1; }
    bool isEmpty();
    bool isFull();
    int pop();
    void push(int x);
};

// Function to check if stack is empty
bool Stack::isEmpty()
{
    if(top == -1)
        return true;
    return false;
}

// Function to check is stack is full
bool Stack::isFull()
{
    if(top == MAX_SIZE - 1)
        return true;
    return false;
}

// Function to pop an element from the stack
int Stack::pop()
{
    if(isEmpty())
    {
        cout << "Stack underflow" << endl;
    }
    else
    {
        int ele = arr[top--];
        return ele;
    }
}

// Function to push an element to the stack
void Stack::push(int ele)
{
    if(isFull())
    {
        cout << "Stack Overflow" << endl;
    }
    else
    {
        arr[++top] = ele;
    }
    
}


// Implementing a derived classs which will support the getMin() function

class SpecialStack: public Stack
{
    Stack min;
public:
    int pop();
    void push(int ele);
    int getMin();
};

void SpecialStack::push(int ele)
{
    if(isEmpty())
    {
        Stack::push(ele);
        min.push(ele);
    }
    else
    {
        Stack::push(ele);
        int temp = min.pop();
        min.push(temp);
        
        // push to min stack only when incoming element is <=
        if(ele <= temp)
            min.push(ele);
    }
}

int SpecialStack::getMin()
{
    int min_stack = min.pop();
    min.push(min_stack);
    
    return min_stack;
}

int SpecialStack::pop()
{
    int top_stack = Stack::pop();
    int top_min = min.pop();
    
    // Push the element only when equal
    if(top_stack!=top_min)
        min.push(top_min);
}


int main()
{
    SpecialStack s;
    s.push(10);
    s.push(20);
    s.push(30);
    cout<<s.getMin()<<endl;
    s.push(5);
    cout<<s.getMin();
    return 0;
}
