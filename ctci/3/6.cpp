/*
 CTCI 3.6) Write a program to sort a stack in ascending order (with biggest items
 on top). You may use additional stacks to hold items, but you may not copy
 the elements into any other data structure  (such as an array). The stack
 supports the following operations: push, pop, peek, and isEmpty
 */

#include <iostream>
#include <stack>

using namespace std;

void sort_stack (stack<int> &s)
{
    // when stack is empty
    if(s.empty())
        return;
    
    // while stack is not empty
    stack<int> temp;
    temp.push(s.top());
    s.pop();
    
    while(!s.empty())
    {
        if(temp.top() < s.top())
        {
            int ele =  s.top();
            s.pop();
            
            while(!temp.empty() && temp.top()<ele)
            {
                s.push(temp.top());
                temp.pop();
            }
            temp.push(ele);
        }
        while(!s.empty() && temp.top() >= s.top())
        {
            temp.push(s.top());
            s.pop();
        }
    }
    
    // Copy back to original stack
    while(!temp.empty())
    {
        s.push(temp.top());
        temp.pop();
    }
}
int main()
{
    stack<int> s;
    s.push(1);
    s.push(3);
    s.push(5);
    s.push(2);
    s.push(2);
    s.push(4);
    sort_stack(s);
    while (!s.empty())
    {
        cout << s.top() << endl;
        s.pop();
    }
    return 0;
}
