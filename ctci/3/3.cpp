/*
 * From Cracking the Coding Interview, 5th edition
 * Problem 3.3
 *
 * Imagine a (literal) stack of plates. If the stack gets too high. it might
 * topple. Therefore, in real life, we would likely start a new stack when the
 * previous stack exceeds some threshold. Implement a data structure SetofStacks
 * that mimics this. SetOfStacks should be composed of several stacks and should
 * create a new stack once the previous one exceeds capacity. SetOfStacks.push()
 * and SetOfStacks.pop() should behave identically to a single stack (that is,
 * pop() should  return the same values as it would if there were just a single
 * stack.
 * FOLLOW UP
 * Implement a function popAt(int index) which performs a pop operation on a
 * specific sub-stack.
 */

#include <iostream>
#include <vector>
#include <stdexcept>

using namespace std;

template <typename T> class SetofStacks
{
    vector<T*> ss;
    int max_size;
    int size;
    
public:
    // Constructor
    SetofStacks(int sz)
    {
        if(sz<=0)
            throw invalid_argument("Invalid Stack Size");
        // Size of each stack
        max_size = sz;
        // current size of all the stacks
        size = 0;
    }
    // Destructor
    ~SetofStacks()
    {
        // till the ss vector is not empty
        while(!ss.empty())
        {
            // delete the element
            delete[] ss.back();
            // delete the container
            ss.pop_back();
        }
    }
    // push function
    void push (T ele)
    {
        // if the this is the first stack or if previous stack is full
        if(size == max_size || size == 0)
        {
            T* temp = new T[max_size];
            // push the new stack on vector
            ss.push_back(temp);
            size = 0;
        }
        // insert the element
        (ss.back())[size++] = ele;
    }
    // pop function: removes from last stack in the set
    void pop()
    {
        if(ss.empty())
            throw invalid_argument ("Empty Stack Set");
        // if this is the first element in the last stack
        if(size == 0)
        {
            // delete it
            delete[] ss.back();
            ss.pop_back();
            // reintialize the size
            size = max_size;
        }
        else
            // if there are more than one element just decrement the size
            size--;
    }
    // function to pop at a given stack
    void popAt(int i)
    {
        if (i<0 || i>=ss.size())
            throw invalid_argument("Invalid Stack Number");
        // if this is last stack
        if(i == ss.size()-1)
        {
            // just pop
            pop();
            return;
        }
        // rearraneg til the second last stack
        for(int j=i+1;j<ss.size()-1; j++)
        {
            ss[j-1][max_size-1] = ss[j][0];
            for(int k=0; k<max_size-1; k++)
                ss[j][k] = ss[j][k+1];
        }
        // rearrage the second last and the last stack
        ss[ss.size()-2][max_size-1] = ss[ss.size()-1][0];
        for(int k=0; k<size-1; k++)
            ss[ss.size()-1][k]=ss[ss.size()-1][k+1];
        size--;
        // if the last stack becomes empty. remove it from vector
        if(size == 0)
        {
            delete[] ss.back();
            ss.pop_back();
            size = max_size;
        }
    }
    void print()
    {
        for(int i=0; i<ss.size(); i++)
        {
            cout << "Stack" << i << ":";
            for(int j=0;j<(i==ss.size()-1?size:max_size);j++)
                cout << ss[i][j] <<" ";
            cout << endl;
        }
        cout << endl;
    }
};


int main() {
    SetofStacks<unsigned int> s(5);
    s.push(10);
    s.push(93);
    s.push(73);
    s.print();
    s.push(83);
    s.pop();
    s.push(74);
    s.push(44);
    s.pop();
    s.push(47);
    s.push(123);
    s.push(773);
    s.popAt(1);
    s.print();
    s.popAt(0);
    s.print();
    s.push(88);
    s.push(97);
    s.push(477);
    s.push(764);
    s.push(92);
    s.push(32);
    s.push(230);
    s.print();
    s.popAt(1);
    s.popAt(0);
    s.print();
    s.popAt(0);
    s.popAt(1);
    s.popAt(0);
    s.pop();
    s.popAt(0);
    s.print();
}
